#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#define u8 uint8_t

/*
rock 1
paper 2
scissor 3
loss 0 
draw 3 
win 6
*/

struct strat {
	u8 op;
	u8 f1; 
	u8 pl; 
	u8 f2;
};

int main() {
	FILE *fp;
	fp = fopen("day2.input", "r");

	struct strat *data; 
	data = calloc(10000, sizeof(u8));
	fread(data, 10000, 1, fp);
	
	int score = 0;
	for(int i=0; i<2500; i++) {
		int t_op, t_pl;
		t_op = (int)(data[i].op)-64;
		t_pl = (int)(data[i].pl)-87;
		printf("%d %d\n", t_op, t_pl);	
		score += t_pl;
		printf("%d\n", score);
		if(t_op-t_pl == 0) {
			score += 3;
		}
		else if(t_op-t_pl == -1 || t_op-t_pl == 2) {
			score += 6;
		}
	}
	printf("total score: %d\n", score);

	return 0;
}

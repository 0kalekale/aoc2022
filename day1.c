// gcc day1.c helpkit.c  

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "helpkit.h"

int main() {
	int a[1024][32];
	size_t len;
	ssize_t read;
	FILE 	*fp; 
	char *buf;

	fp = fopen("day1.input", "r");
	buf = malloc(4096);	

	int i, j = 0; 
	while((read = getline(&buf, &len, fp)) != -1) {
		if(strcmp(buf, "\n") == 0) {
			i++; // next elf
			j = 0;
		}
		else {
			a[i][j] = atoi(buf);
			j++; // next food 
		}
	}
	
	int x, y;
	int sums[512];

	for (x; x<i; x++) {
		printf("elf %d: \n", x);
		for (y=0; y<32; y++) {
			sums[x]+=a[x][y];	
		}
	}

	quicksort(sums, 0, x-1);
	for(int z=0; z<x; z++) {
		printf("%d\n", sums[z]);	
	}
	
	return 0;
}
